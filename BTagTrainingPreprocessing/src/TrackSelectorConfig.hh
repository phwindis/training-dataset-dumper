#ifndef TRACK_SELECTOR_CONFIG_HH
#define TRACK_SELECTOR_CONFIG_HH

struct TrackSelectorConfig
{
  float pt_minumum = 1e3;
  float d0_maximum = 1;
  float z0_maximum = 1.5;
};

#endif
