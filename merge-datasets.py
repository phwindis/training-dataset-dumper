#!/usr/bin/env python3

"""Make a hybrid of two files"""

_njet_help='number of jets to include from each sample'

from argparse import ArgumentParser
import h5py
import numpy as np

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('file_one')
    parser.add_argument('file_two')
    parser.add_argument('-o', '--output-file', default='hybrid.h5')
    parser.add_argument('-n', '--n-jets', default=1000,
                        help=_njet_help, type=int)
    split = parser.add_mutually_exclusive_group()
    split.add_argument('--even', action='store_true')
    split.add_argument('--odd', action='store_true')
    return parser.parse_args()

def run():
    args = get_args()
    sl = slice(0, args.n_jets, None)
    if args.even:
        sl = slice(0, args.n_jets*2, 2)
    elif args.odd:
        sl = slice(1, args.n_jets*2 + 1, 2)

    with h5py.File(args.file_one,'r') as f1:
        entries1 = f1['jets'][sl]
    with h5py.File(args.file_two,'r') as f2:
        entries2 = f2['jets'][sl]

    cat = np.concatenate([entries1, entries2])
    np.random.shuffle(cat)
    with h5py.File(args.output_file,'w') as out_file:
        out_file.create_dataset('jets', data=cat, compression='gzip')

if __name__ == '__main__':
    run()
